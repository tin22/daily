#!/home/tin/.cache/pypoetry/virtualenvs/daily-hbNZGzX3-py3.10/bin/python
# _*_ coding: utf-8 _*_

import atexit
import sys
from datetime import datetime
from pathlib import Path

from PyQt6.QtWidgets import QApplication
from loguru import logger

import daily_face

DAY_0 = datetime(2024, 7, 4, 0, 0, 0, 0)
logger.add(Path(__file__).parent / 'daily.log',
           format="{time:YYYY-MM-DD at HH:mm:ss} | {level} | {module} | {message}",
           level="INFO", rotation='500 KB')

# logger.info('')
logger.info('==== Program started. ====')  # Executing interpreter: {sys.executable}')
now = datetime.now()
delta = now - DAY_0
logger.info('Days have passed since ' + DAY_0.strftime("%d.%m.%Y") + ': ' + str(delta.days))


def endlog():
    logger.info('Days have passed since ' + DAY_0.strftime("%d.%m.%Y") + ': ' + str(delta.days))
    logger.info('==== Program ended. ==== \n')


atexit.register(endlog)

if __name__ == '__main__':
    logger.debug('Main loop in progress.')
    logger.debug('shown app..')
    logger.debug('The program is ready and running')
    app = QApplication(sys.argv)
    application = daily_face.DailyFace()
    application.show()
    sys.exit(app.exec())
