#!/home/tin/.cache/pypoetry/virtualenvs/daily-hbNZGzX3-py3.10/bin/python
# _*_ coding: utf-8 _*_

import sys
from datetime import datetime

import requests
from loguru import logger


class DataModule:
    """ Class for accessing data via API.
    """

    def __init__(self):
        # FORMAT = '%(asctime)s: %(name)s: %(levelname)s: %(message)s'
        # filename = str(os.path.dirname(os.path.abspath(__file__))) + '/daily.log'
        # logging.basicConfig(filename=filename, level=logging.INFO,
        #                     format=FORMAT, datefmt='%Y-%m-%d %H:%M:%S')
        # self.logger = logging.getLogger(__name__)

        self.API_URL = 'http://lifedesc/'
        # self.API_URL = 'http://localhost:8000/'  # For the serverless option (ld_start.fish)

    def get_day(self, desired_date: str) -> dict:
        """ Get the 'day' object from the database.
            The data for desired_date is selected from the database. If there is no data
            and desired_date matches current date, an empty day is created with that
            date. Any other day, if there is no data, the program stops working with the
            corresponding message in the log file.
            INPUT: desired_date <str>;
            OUTPUT: <dict> - 'day' object.
        """
        url = self.API_URL + 'daily/' + desired_date + '/'
        res = requests.get(url)

        if res.status_code == 404 and \
                desired_date <= datetime.today().strftime("%Y-%m-%d"):  # No data and desired_date is today or earlier

            # Сreate a new day
            new_day = {
                'date': desired_date,
                'push_ups': 0,
                'squates': 0,
                'weight': 0.0,
                'bmi': 0.0,
                'steps': 0,
                'deep_sleeptime': '00:00:00',
                'sleeptime': '00:00:00',
                'dss': '0%',  # Added % to avoid an error 'TypeError: 'int' object is not subscriptable'
                'temperature': '35.0',
                'sa': 0,
                'thoughts': [],
            }
            try:
                r = requests.post('http://lifedesc/last/', new_day, auth=('admin', '123'))
                # r = requests.post('http://localhost:8000/last/', new_day, auth=('admin', '123'))  # For the serverless option
                if r.status_code == 201:
                    logger.info('New day created. Status code = 201.')
                    logger.debug('Payload: ' + str(new_day))
                else:
                    logger.exception('Something went wrong.')
            except:
                logger.exception('Something went wrong.')
                sys.exit()
            return new_day  # TODO: Это не правильно. По сути возвращается не то что есть, а то что должно быть.

        if res.status_code == 200:
            # logger.info('Data received.')
            return res.json()

        # desired_date is not today and there is no data for desired_date in the database
        logger.exception(f'Status code = {str(res.status_code)}. There is no data for the requested date '
                         f'({desired_date}) and there is no reason to start a new day. Abnormal termination.')
        sys.exit()

    def get_all_tags(self) -> object:
        """ Get all possible tags
            OUTPUT: <dict> - Dictionary with all possible tags.
        """
        tags = {}
        url = self.API_URL + 'alltags/'
        res = requests.get(url)
        for tag in res.json():
            tags[tag['tag']] = tag['id']  # ???
        return tags

    def get_last_days(self) -> list:
        url = self.API_URL + 'last/'
        res = requests.get(url).json()
        return res

    def put_day(self, desired_date: str, new_day: dict) -> bool:
        """ Put the 'day' object in the database.
            INPUT: new_day <dict> - Dictionary with data;
            OUTPUT: <bool> - True, if the data was written successfully.
        """
        url = self.API_URL + 'daily/' + desired_date + '/'

        res = requests.put(url, json=new_day)
        # logger.info(f'On {desired_date} will be written: {str(new_day)}')
        # return True

        if res.status_code == 200:
            logger.info('Data sent successfully. Status code is ' + str(res.status_code) + '.')
            logger.debug('Payload: ' + str(new_day))
            return True
        else:
            logger.error('Something went wrong. Status code is ' + str(res.status_code) + '.')
            return False
