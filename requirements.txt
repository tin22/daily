alabaster==0.7.12
altgraph==0.17.3
Babel==2.11.0
certifi==2022.9.24
chardet==5.0.0
charset-normalizer==2.1.1
contourpy==1.0.6
cycler==0.11.0
docutils==0.19
fonttools==4.38.0
idna==3.4
imagesize==1.4.1
kiwisolver==1.4.4
loguru==0.6.0
MarkupSafe==2.1.1
matplotlib==3.6.2
numpy==1.23.5
packaging==21.3
Pillow==9.3.0
Pygments==2.13.0
pyparsing==3.0.9
PyQt6==6.4.0
PyQt6-Qt6==6.4.1
PyQt6-sip==13.4.0
python-dateutil==2.8.2
pytz==2022.6
requests==2.28.1
six==1.16.0
snowballstemmer==2.2.0
urllib3==1.26.13
